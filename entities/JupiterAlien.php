<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JupiterAlien
 *
 * @author pabhoz
 */
class JupiterAlien extends BadAlien{
    
    private $nombre, $edad, $especie;
    private $planeta = "Jupiter";
    
    function __construct($nombre, $edad, $especie) {
        parent::__construct($nombre, $edad, $especie, $this->planeta);
    }
    
    public function interact() {
        $s = parent::interact();
        return str_replace("s", "sh",$s);
    }

}
