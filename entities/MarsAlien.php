<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarsAlien
 *
 * @author pabhoz
 */
class MarsAlien extends GoodAlien{
    private $nombre, $edad, $especie;
    private $planeta = "Marte";
    
    function __construct($nombre, $edad, $especie) {
        parent::__construct($nombre, $edad, $especie, $this->planeta);
    }

    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function getEspecie() {
        return $this->especie;
    }

    function getPlaneta() {
        return $this->planeta;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setEspecie($especie) {
        $this->especie = $especie;
    }

    function setPlaneta($planeta) {
        $this->planeta = $planeta;
    }
    
}
